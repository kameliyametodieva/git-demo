package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
public class NavigationPage extends BasePage {

    public NavigationPage() {
        super("base.url");
    }

    public final String homeButton = "navigation.Home";
    public final String signInButton = "navigation.Login";
    public final String signUpButton = "navigation.SignUp";
    //create all navigation selectors
}