package com.telerikacademy.finalproject.enums;

public enum BrowserType {
    CHROME,
    FIREFOX,
    OPERA
}
