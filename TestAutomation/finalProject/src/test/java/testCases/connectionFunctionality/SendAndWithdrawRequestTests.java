package testCases.connectionFunctionality;

import com.telerikacademy.finalproject.pages.HomePage;
import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.UsersPage;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SendAndWithdrawRequestTests extends BaseTest {

    public final String user2XPath = "users.User2xpath";
    public final String usernameUser2 = "usernameUser2";


    @BeforeClass
    public static void loginUser () {
        NavigationPage navPage = new NavigationPage();
        navPage.assertPageNavigated();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage logInPage = new LogInPage();
        logInPage.assertPageNavigated();
        logInPage.loginUser("usernameUser1", "passwordUser1");

    }

    @Test
    public void c1_loggedUser_SendsConnectionRequest_ToUser2 (){
        UsersPage userPage = new UsersPage();
        userPage.navigateToPage();
        userPage.searchUser(usernameUser2, user2XPath);
        userPage.sendConnectionRequest(user2XPath);
        userPage.assertRequestForConnectionIsSent();
    }

    @Test
    public void c2_loggedUser_WithdrawSentRequest_ToUser2(){
        UsersPage userPage = new UsersPage();
        userPage.navigateToPage();
        userPage.searchUser(usernameUser2, user2XPath);
        userPage.withdrawConnectionRequest(user2XPath);
        userPage.assertWithdrawOfConnectionRequest();
    }
}
