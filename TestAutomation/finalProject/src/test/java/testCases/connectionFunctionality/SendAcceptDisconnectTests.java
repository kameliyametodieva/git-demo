package testCases.connectionFunctionality;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.UsersPage;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class SendAcceptDisconnectTests {

    public final String user1XPath = "users.User1xpath";
    public final String usernameUser1 = "usernameUser1";
    public final String user2XPath = "users.User2xpath";
    public final String usernameUser2 = "usernameUser2";

    public UsersPage userPage;

    @Before
    public void beforeTest() {
        userPage = new UsersPage();
        userPage.navigateToPage();
        userPage.assertPageNavigated();
    }

    @Test
    public void d1_loginUser1() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage logInPage = new LogInPage();
        logInPage.loginUser("usernameUser1", "passwordUser1");
    }

    @Test
    public void d2_loggedUser_1SendsConnectionRequest_ToUser2() {
        userPage.searchUser(usernameUser2, user2XPath);
        userPage.sendConnectionRequest(user2XPath);
        userPage.assertRequestForConnectionIsSent();
    }

    @Test
    public void d3_logoutUser1() {
        userPage.logoutUser();
        userPage.assertUserLogout();
    }

    @Test
    public void d4_loginUser2() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage logInPage = new LogInPage();
        logInPage.loginUser("usernameUser2", "passwordUser2");
    }

    @Test
    public void d5_loggedUser_2ConfirmConnectionRequest_ToUser1() {
        userPage.confirmConnectionRequest(user1XPath);
        userPage.assertConfirmationOfConnectionRequest();
    }

    @Test
    public void d6_loggedUser_2DisconnectUser1 (){
        userPage.searchUser(usernameUser1, user1XPath);
        userPage.disconnectUser(user1XPath);
        userPage.assertDisconnection();
    }

    @Test
    public void d7_loggedOut_User2 () {
        userPage.logoutUser();
        userPage.assertUserLogout();
    }

}
