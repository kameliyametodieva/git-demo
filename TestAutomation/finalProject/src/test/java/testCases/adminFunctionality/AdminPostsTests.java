package testCases.adminFunctionality;

import com.telerikacademy.finalproject.pages.*;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdminPostsTests extends BaseTest {


    @BeforeClass
    public static void loginAdmin_After_fillingAdminNameAndPassword() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.loginUser("usernameAdmin", "passwordAdmin");



    }
    @Test
    public void a_edit_UserPost(){
        HomePage homePage = new HomePage();
        actions.hoverOnElement(homePage.settingsButton);
        actions.clickElement("post.Admin");

        EditPostPage editPostPage = new EditPostPage();
        editPostPage.assertPageNavigated();
        actions.assertElementPresent("post.checkPage");
        //actions.assertElementPresent("post.searchField");
        //actions.clickElement("post.searchField");
        //actions.typeValueInField("postOne","post.searchField" );
        //actions.clickElement("post.searchButton");
        //actions.isElementPresentUntilTimeout("post.editTitle",10);
        actions.clickElement("post.editTitle",2000);
        actions.assertElementPresent("post.editButton");
        actions.clickElement("post.editButton");
        actions.assertElementPresent("post.titleField");
        actions.clearFieldText("post.titleField");
        actions.typeValueInField("SeleniumTitle", "post.titleField");
        actions.clickElement("post.saveButton",1000);

    }

    @Test
    public void b_delete_UserPost(){

        HomePage homePage = new HomePage();
        actions.hoverOnElement(homePage.settingsButton);
        actions.clickElement("post.Admin");

        EditPostPage editPostPage = new EditPostPage();
        editPostPage.assertPageNavigated();
        actions.assertElementPresent("post.checkPage");
        actions.clickElement("post.editTitle",2000);
        actions.assertElementPresent("post.editButton");
        actions.clickElement("post.editButton");
        actions.assertElementPresent("post.deleteButton");
        actions.clickElement("post.deleteButton",1000);
        actions.assertElementPresent("post.confirmationDeleteButton");
        actions.clickElement("post.confirmationDeleteButton");
    }





}
