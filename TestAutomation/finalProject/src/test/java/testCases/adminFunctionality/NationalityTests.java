package testCases.adminFunctionality;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class NationalityTests extends BaseTest {


    @BeforeClass
    public static void loginAdmin_After_fillingAdminNameAndPassword() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.loginUser("usernameAdmin", "passwordAdmin");
    }


    @Test
    public void b1_create_Nationality() {
        HomePage homePage = new HomePage();
        actions.hoverOnElement(homePage.settingsButton);
        actions.clickElement(homePage.nationalitiesButton);
        NationalitiesPage nationalitiesPage = new NationalitiesPage();
        nationalitiesPage.assertPageNavigated();
        actions.assertElementPresent("natPage.title");
        actions.isElementPresentUntilTimeout("nationalities.addButton", 10);
        actions.clickElement("nationalities.addButton");
        actions.isElementPresentUntilTimeout("nationalities.nameField", 20);
        actions.typeValueInField(Utils.getUIMappingByKey("nationalities.newNameNationality"), "nationalities.nameField");
        actions.isElementPresentUntilTimeout("nationalities.saveButton", 20);
        actions.clickElement("nationalities.saveButton");



    }

    @Test
    public void b2_assert_NationalityIsCreated() {

        actions.isElementPresentUntilTimeout("nationalities.nationalityNameCheck", 20);
        actions.assertElementPresent("nationalities.nationalityNameCheck");


    }

    @Test
    public void b3_update_Nationality() {


        actions.isElementPresentUntilTimeout("nationalities.editButton", 10);
        actions.clickElement("nationalities.editButton");
        actions.isElementPresentUntilTimeout("nationalities.nameField", 10);
        actions.clearFieldText("nationalities.nameField");actions.typeValueInField(Utils.getUIMappingByKey("nationalities.nationalityUpdateName"), "nationalities.nameField");
        actions.isElementPresentUntilTimeout("nationalities.saveButton", 10);
        actions.clickElement("nationalities.saveButton");
    }

    @Test
    public void b4_assert_NationalityIsUpdate(){

        actions.assertElementPresent("natPage.title");
        actions.isElementPresentUntilTimeout("nationalities.nationalityIsUpdated",20);
        actions.assertElementPresent("nationalities.nationalityIsUpdated");
    }

    @Test
    public void b5_delete_Nationality(){
        actions.isElementPresentUntilTimeout("nationalities.nationalityNameCheck", 10);
        actions.clickElement("nationalities.nationalityNameCheck");
        actions.isElementPresentUntilTimeout("nationalities.deleteButton",10);
        actions.clickElement("nationalities.deleteButton");
        actions.isElementPresentUntilTimeout("nationalities.confirmDeletionButton",10);
        actions.clickElement("nationalities.confirmDeletionButton");
    }

    @Test
    public void b6_assert_NationalityIsDeleted(){
        actions.isElementPresentUntilTimeout("nationalities.editButton", 10);
        actions.assertElementIsNotPresent("nationalities.editButton", "nationalities.nationalityUpdateName");
}
}
