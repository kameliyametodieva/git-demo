package testCases;

import com.telerikacademy.finalproject.pages.HomePage;
import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginTests extends BaseTest {


    @Test
    public void a1_navigateToHome_UsingNavigation(){
        NavigationPage navPage = new NavigationPage();
        actions.clickElement(navPage.homeButton);
        navPage.assertPageNavigated();
    }


    @Test
    public void a2_navigationPage_navigate_ToLogin() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
    }

    @Test
    public void a3_loginPage_verify_Elements() throws InterruptedException {
        LogInPage loginPage = new LogInPage();
        actions.assertElementPresent(loginPage.userNameField);
        actions.assertElementPresent(loginPage.passwordField);
    }
    @Test
    public void a4_user_canNotLogIn_With_EmptyUsernameAndCorrectPassword(){
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
        String password = Utils.getConfigPropertyByKey("password");
        loginPage.actions.typeValueInField(password,loginPage.passwordField);
        actions.clickElement(loginPage.loginButton);
        loginPage.assertPageNavigated();
        actions.assertElementPresent(loginPage.wrongLoginMessage);

    }
    @Test
    public void a5_user_canNotLogin_With_EmptyPasswordAndCorrectUsername(){
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
        String username = Utils.getConfigPropertyByKey("username");
        actions.typeValueInField(username,loginPage.userNameField);
        actions.clickElement(loginPage.loginButton);
        loginPage.assertPageNavigated();
        actions.assertElementPresent(loginPage.wrongLoginMessage);
    }
    @Test
    public void a6_user_canNotLogin_With_WrongPasswordAndCorrectUsername(){
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
        String username = Utils.getConfigPropertyByKey("username");
        actions.typeValueInField(username,loginPage.userNameField);
        actions.typeValueInField("wrongPass",loginPage.passwordField);
        actions.clickElement(loginPage.loginButton);
        loginPage.assertPageNavigated();
        actions.assertElementPresent(loginPage.wrongLoginMessage);

    }
    @Test
    public void a7_user_canNotLogin_With_WrongUsernameAndCorrectPassword(){
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
        actions.typeValueInField("wrongUserName",loginPage.userNameField);
        String password = Utils.getConfigPropertyByKey("password");
        actions.typeValueInField(password,loginPage.passwordField);
        actions.clickElement(loginPage.loginButton);
        loginPage.assertPageNavigated();
        actions.assertElementPresent(loginPage.wrongLoginMessage);

    }

    @Test
    public void a8_user_successfully_loggingIn(){
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
        loginPage.loginUser("username", "password");

    }



}
